--[[

     Holo Awesome WM theme 3.0
     github.com/lcpz

--]]

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi

local string, os = string, os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}
theme.default_dir                               = require("awful.util").get_themes_dir() .. "default"
theme.icon_dir                                  = os.getenv("HOME") .. "/.config/awesome/themes/holo/icons"
theme.wallpaper                                 = os.getenv("HOME") .. "/.config/awesome/themes/wallpaper/lambo.jpg"
theme.font                                      = "Roboto Bold 10"
theme.taglist_font                              = "Roboto Condensed Regular 8"
theme.fg_normal                                 = "#454545"
theme.fg_focus                                  = "#BBDDFF"
theme.bg_focus                                  = "#454545"
theme.bg_normal                                 = "#DDDDDD"
theme.b_bar_opacity                             = "AA"
theme.fg_urgent                                 = "#DD4545"
theme.bg_urgent                                 = "#006B8E"
theme.border_width                              = dpi(1)
theme.border_normal                             = "#454545"
theme.border_focus                              = "#BBDDFF"
theme.border_radius                             = dpi(5)
theme.taglist_fg_focus                          = "#FFFFFF"
theme.tasklist_bg_normal                        = "transparent"
theme.tasklist_fg_focus                         = theme.fg_normal
theme.tasklist_fg_normal                        = theme.bg_normal
theme.menu_height                               = dpi(20)
theme.menu_width                                = dpi(160)
theme.menu_icon_size                            = dpi(32)
theme.awesome_icon                              = theme.icon_dir .. "/awesome_icon_white.png"
theme.awesome_icon_launcher                     = theme.icon_dir .. "/awesome_icon.png"
theme.taglist_squares_sel                       = theme.icon_dir .. "/square_sel.png"
theme.taglist_squares_unsel                     = theme.icon_dir .. "/square_unsel.png"
theme.spr_small                                 = theme.icon_dir .. "/spr_small.png"
theme.spr_very_small                            = theme.icon_dir .. "/spr_very_small.png"
theme.spr_right                                 = theme.icon_dir .. "/spr_right.png"
theme.spr_bottom_right                          = theme.icon_dir .. "/spr_bottom_right.png"
theme.spr_left                                  = theme.icon_dir .. "/spr_left.png"
theme.bar                                       = theme.icon_dir .. "/bar.png"
theme.bottom_bar                                = theme.icon_dir .. "/bottom_bar.png"
theme.mpdl                                      = theme.icon_dir .. "/mpd.png"
theme.mpd_on                                    = theme.icon_dir .. "/mpd_on.png"
theme.prev                                      = theme.icon_dir .. "/prev.png"
theme.nex                                       = theme.icon_dir .. "/next.png"
theme.stop                                      = theme.icon_dir .. "/stop.png"
theme.pause                                     = theme.icon_dir .. "/pause.png"
theme.play                                      = theme.icon_dir .. "/play.png"
theme.clock                                     = theme.icon_dir .. "/clock.png"
theme.calendar                                  = theme.icon_dir .. "/cal.png"
theme.cpu                                       = theme.icon_dir .. "/cpu.png"
theme.net_up                                    = theme.icon_dir .. "/net_up.png"
theme.net_down                                  = theme.icon_dir .. "/net_down.png"
theme.layout_tile                               = theme.icon_dir .. "/tile.png"
theme.layout_tileleft                           = theme.icon_dir .. "/tileleft.png"
theme.layout_tilebottom                         = theme.icon_dir .. "/tilebottom.png"
theme.layout_tiletop                            = theme.icon_dir .. "/tiletop.png"
theme.layout_fairv                              = theme.icon_dir .. "/fairv.png"
theme.layout_fairh                              = theme.icon_dir .. "/fairh.png"
theme.layout_spiral                             = theme.icon_dir .. "/spiral.png"
theme.layout_dwindle                            = theme.icon_dir .. "/dwindle.png"
theme.layout_max                                = theme.icon_dir .. "/max.png"
theme.layout_fullscreen                         = theme.icon_dir .. "/fullscreen.png"
theme.layout_magnifier                          = theme.icon_dir .. "/magnifier.png"
theme.layout_floating                           = theme.icon_dir .. "/floating.png"
theme.tasklist_plain_task_name                  = false
theme.tasklist_icon_size                        = dpi(10)
theme.useless_gap                               = dpi(1)
theme.titlebar_close_button_normal              = theme.default_dir.."/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.default_dir.."/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.default_dir.."/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.default_dir.."/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.default_dir.."/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.default_dir.."/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.default_dir.."/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.default_dir.."/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.default_dir.."/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.default_dir.."/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.default_dir.."/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.default_dir.."/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.default_dir.."/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.default_dir.."/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.default_dir.."/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.default_dir.."/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.default_dir.."/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.default_dir.."/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.default_dir.."/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.default_dir.."/titlebar/maximized_focus_active.png"

-- theme.musicplr = string.format("%s -e mpv", awful.util.terminal)

-- awesome-wm-widgets
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local word_clock = require("awesome-wm-widgets.word-clock-widget.word-clock") -- obsolète
local ram_widget = require("awesome-wm-widgets.ram-widget.ram-widget")
local fs_widget = require("awesome-wm-widgets.fs-widget.fs-widget")
local logout_menu_widget = require("awesome-wm-widgets.logout-menu-widget.logout-menu")
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar")
local docker_widget = require("awesome-wm-widgets.docker-widget.docker")
--
-- WidgetA
local sound_widget = require("widgetsA.wifi")
--

--Launchbar
local launchbar = require("themes.launchbar")
local mylaunchbar = launchbar("~/Desktop/")
--

local markup = lain.util.markup
-- local blue   = "#80CCE6"
local space3 = markup.font("Roboto 3", " ")

-- Clock and calendar
local mytextclock = wibox.widget.textclock(markup(theme.bg_normal, space3 .. "%H:%M   " .. markup.font("Roboto 4", " ")))
mytextclock.font = theme.font
local clock_icon = wibox.widget.imagebox(theme.clock)
local clockwidget = wibox.container.margin(clockbg, dpi(0), dpi(3), dpi(5), dpi(5))

local cw = calendar_widget({
    theme = 'dark',
    start_sunday = true,
    radius = 7.5,
})
mytextclock:connect_signal("button::press",
    function(_, _, _, button)
        if button == 1 then cw.toggle() end
    end)
--

-- CPU
local cpu_icon = wibox.widget.imagebox(theme.cpu)
local cpu = lain.widget.cpu({
    fg = theme.bg_normal,
    settings = function()
        widget:set_markup(space3 .. markup.font(theme.font, "CPU " .. cpu_now.usage
                          .. "% ") .. markup.font("Roboto 5", " "))
    end
})
local cpubg = wibox.container.background(cpu.widget, "transparent", gears.shape.rectangle)
local cpuwidget = wibox.container.margin(cpubg, dpi(0), dpi(0), dpi(5), dpi(5))


-- Launcher
local mylauncher = awful.widget.button({ image = theme.awesome_icon })
mylauncher:connect_signal("button::press", function() awful.util.mymainmenu:toggle() end)

-- Separators
local first = wibox.widget.textbox('<span font="Roboto 7"> </span>')
local spr_small = wibox.widget.imagebox(theme.spr_small)
local spr_very_small = wibox.widget.imagebox(theme.spr_very_small)
local spr_right = wibox.widget.imagebox(theme.spr_right)
local spr_bottom_right = wibox.widget.imagebox(theme.spr_bottom_right)
local spr_left = wibox.widget.imagebox(theme.spr_left)
local bar = wibox.widget.imagebox(theme.bar)
local bottom_bar = wibox.widget.imagebox(theme.bottom_bar)

local barcolor  = gears.color({
    type  = "linear",
    from  = { dpi(32), 0 },
    to    = { dpi(32), dpi(32) },
    stops = { {0, theme.bg_focus}, {0.25, "#505050"}, {1, theme.bg_focus} }
})
local dockshape = function(cr, width, height)
    gears.shape.partially_rounded_rect(cr, width, height, false, true, true, false, 6)
end

function theme.bottom_wibox(s)
    -- -- Create the vertical wibox

    local b_b_h = dpi(30)
    -- Create the bottom wibox
    s.mybottomwibox = wibox({
        screen = s,
        x = 0,
        y = ((s.workarea.height + 20) - dpi(2.5)),
        width = s.workarea.width,
        position = "bottom", screen = s,
        border_width = dpi(0),
        height = b_b_h,
        bg = theme.bg_focus .. theme.b_bar_opacity,
        visible = true,
        ontop = true,
    })
    -- s.borderwibox = awful.wibar({ position = "bottom", screen = s, height = dpi(1), bg = theme.fg_focus, x = dpi(0), y = dpi(33)})

    if s.index > 1 and s.mybottomwibox.y == 0 then
        s.mybottomwibox.y = screen[1].mybottomwibox.y
    end

    -- Add widgets to the bottom wibox
    s.mybottomwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.container.margin(mylauncher, dpi(2.5), dpi(2.5), dpi(5), dpi(5)),
            mylaunchbar,
        },
        wibox.container.margin(s.mytasklist, dpi(2.5), dpi(2.5), dpi(2.5), dpi(2.5)), -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            -- spr_bottom_right,
            -- bottom_bar,
            -- cpu_icon,
            -- cpuwidget,
            -- bottom_bar,
            -- calendar_icon,
            -- calendarwidget,
            -- bottom_bar,
            -- clock_icon,
        },
    }
 

    -- Add toggling functionalities
    s.docktimer = gears.timer{ timeout = 2 }
    s.docktimer:connect_signal("timeout", function()
        local s = awful.screen.focused()
        s.mybottomwibox.y = ((s.workarea.height + 20) - dpi(2.5))
        if s.docktimer.started then
            s.docktimer:stop()
        end
    end)
    tag.connect_signal("property::selected", function(t)
        local s = t.screen or awful.screen.focused()
        s.mybottomwibox.y = ((s.workarea.height + 20) - b_b_h)
        gears.surface.apply_shape_bounding(s.mybottomwibox, dockshape)
        if not s.docktimer.started then
            s.docktimer:start()
        end
    end)

    s.mybottomwibox:connect_signal("mouse::leave", function()
        local s = awful.screen.focused()
        s.mybottomwibox.y = ((s.workarea.height + 20) - dpi(2.5))
    end)

    s.mybottomwibox:connect_signal("mouse::enter", function()
        local s = awful.screen.focused()
        s.mybottomwibox.y = ((s.workarea.height + 20) - b_b_h)
        gears.surface.apply_shape_bounding(s.mybottomwibox, dockshape)
    end)
end

function theme.at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake({ app = awful.util.terminal })

    -- If wallpaper is a function, call it with the screen
    
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
    --

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts)
    --

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({}, 1, function () awful.layout.inc( 1) end),
                           awful.button({}, 2, function () awful.layout.set( awful.layout.layouts[1] ) end),
                           awful.button({}, 3, function () awful.layout.inc(-1) end),
                           awful.button({}, 4, function () awful.layout.inc( 1) end),
                           awful.button({}, 5, function () awful.layout.inc(-1) end)))
    --

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons, { bg_focus = theme.bg_normal .. theme.b_bar_opacity , shape = gears.shape.rounded_rect, shape_border_width = 1, shape_border_color = theme.tasklist_bg_normal, align = "center" })
    -- gears.surface.apply_shape_bounding(s.mytasklist, beautiful.clientshape)
    --

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(20), bg = "tansparent" })
    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        expand = "none",
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.container.margin(cpuwidget, dpi(2.5), dpi(0), dpi(0), dpi(0)),
            ram_widget({
                timeout = 0.5,
                color_used = theme.bg_normal .. theme.b_bar_opacity,
                color_free = theme.fg_normal .. theme.b_bar_opacity,
                color_buf  = theme.bg_urgent .. theme.b_bar_opacity,
            }),
            fs_widget({
                widget_bar_color            = theme.bg_normal,
                widget_border_color         = theme.bg_normal .. theme.b_bar_opacity,
                widget_background_color     = theme.fg_normal .. theme.b_bar_opacity,
                popup_bar_color             = theme.bg_normal,
                popup_bg                    = theme.fg_normal .. theme.b_bar_opacity,
                popup_fg                    = theme.bg_urgent,
                popup_border_color          = theme.bg_normal,
                popup_bar_background_color  = theme.fg_normal .. theme.b_bar_opacity,
                popup_bar_border_color      = theme.bg_normal .. theme.b_bar_opacity,
            }),
            s.mypromptbox,
        },
            
        { -- Middle widgets
            layout = wibox.layout.fixed.horizontal,
            -- word_clock{},
            -- gitlab_widget{
            --     host = 'https://gitlab.ehacko.com',
            --     access_token = 'glpat-YNoH7fyZGihy4cYkYWDc'
            -- },
            mytextclock,
            docker_widget(),
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --  wibox.widget { nil, nil, theme.mpd.widget, layout = wibox.layout.align.horizontal },
            wibox.container.margin(volume_widget{
                widget_type = 'arc',
                size = 20,
                bg_color = theme.bg_normal.."75",
                main_color = theme.bg_normal,
                device = "default",
                step = 5,
                -- mixer_cmd = 'alsamixer',
            }, dpi(5), dpi(5), dpi(0), dpi(0)),
            wibox.container.margin(logout_menu_widget({
                bg_normal = theme.fg_normal .. theme.b_bar_opacity,
                fg_normal = theme.bg_normal,
            }), dpi(5), dpi(5), dpi(0), dpi(0)),
            wibox.widget.systray(),
        },
    }
    --

    -- init bottom wibox
    gears.timer.delayed_call(theme.bottom_wibox, s)
end

return theme

return function theme.hidden_wibox(s)
    -- Create the vertical wibox
    s.myhiddenwibox = wibox({ screen = s, x=0, y=s.workarea.height + s.mywibox.height - 2, width = s.workarea.width, height = dpi(35), fg = theme.fg_normal, bg = theme.bg_normal .. "75", ontop = true, visible = true, type = "dock" })

    if s.index > 1 and s.myhiddenwibox.y == 0 then
        s.myhiddenwibox.y = screen[1].myhiddenwibox.y
    end

    -- Add widgets to the hidden wibox
    s.myhiddenwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
        },
        wibox.container.margin(s.mytasklist, dpi(1.25), dpi(1.25), dpi(2.5), dpi(2.5)), -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            -- wibox.container.margin(wibox.widget.systray(), dpi(5), dpi(5), dpi(5), dpi(5)),
        },
    }

    -- Add toggling functionalities
    s.docktimer = gears.timer{ timeout = 2 }
    s.docktimer:connect_signal("timeout", function()
        local s = awful.screen.focused()
        s.myhiddenwibox.y = s.workarea.height + (s.mywibox.height - 2)
        if s.docktimer.started then
            s.docktimer:stop()
        end
    end)
    tag.connect_signal("property::selected", function(t)
        local s = t.screen or awful.screen.focused()
        s.myhiddenwibox.y = s.workarea.height + s.mywibox.height - s.myhiddenwibox.height
        gears.surface.apply_shape_bounding(s.myhiddenwibox, dockshape)
        if not s.docktimer.started then
            s.docktimer:start()
        end
    end)

    s.myhiddenwibox:connect_signal("mouse::leave", function()
        local s = awful.screen.focused()
        s.myhiddenwibox.y = s.workarea.height + s.mywibox.height - 2
    end)

    s.myhiddenwibox:connect_signal("mouse::enter", function()
        local s = awful.screen.focused()
        s.myhiddenwibox.y = s.workarea.height + s.mywibox.height - s.myhiddenwibox.height
        -- gears.surface.apply_shape_bounding(s.myhiddenwibox, dockshape)
    end)
end

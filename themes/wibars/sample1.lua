-- Wifi carrier and signal strength
local wificon = wibox.widget.imagebox(theme.wifidisc)
local wifitooltip = awful.tooltip({
    objects = { wificon },
    margin_leftright = dpi(15),
    margin_topbottom = dpi(15)
})
wifitooltip.wibox.fg = theme.fg_normal
wifitooltip.textbox.font = theme.font
wifitooltip.timeout = 0
wifitooltip:set_shape(function(cr, width, height)
    gears.shape.infobubble(cr, width, height, corner_radius, arrow_size, width - dpi(120))
end)
local mywifisig = awful.widget.watch(
    { awful.util.shell, "-c", "awk 'NR==3 {printf(\"%d-%.0f\\n\",$2, $3*10/7)}' /proc/net/wireless; iw dev wlan0 link" },
    2,
    function(widget, stdout)
        local carrier, perc = stdout:match("(%d)-(%d+)")
        local tiptext = stdout:gsub("(%d)-(%d+)", ""):gsub("%s+$", "")
        perc = tonumber(perc)

        if carrier == "1" or not perc then
            wificon:set_image(theme.wifidisc)
            wifitooltip:set_markup("No carrier")
        else
            if perc <= 5 then
                wificon:set_image(theme.wifinone)
            elseif perc <= 25 then
                wificon:set_image(theme.wifilow)
            elseif perc <= 50 then
                wificon:set_image(theme.wifimed)
            elseif perc <= 75 then
                wificon:set_image(theme.wifihigh)
            else
                wificon:set_image(theme.wififull)
            end
            wifitooltip:set_markup(tiptext)
        end
    end
)
wificon:connect_signal("button::press", function() awful.spawn(string.format("%s -e wavemon", awful.util.terminal)) end)


s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    expand = "none",
    { -- Left widgets
       layout = wibox.layout.fixed.horizontal,
       s.mylayoutbox,
       cpuwidget,s
       s.mypromptbox,
       tspace1,
    },
    { -- Middle widgets
        layout = wibox.layout.flex.horizontal,
        max_widget_size = 1500,
        mytextclock
    },
    { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
       --  wibox.widget { nil, nil, theme.mpd.widget, layout = wibox.layout.align.horizontal },
        wibox.widget { nil, nil, theme.mpd.widget, layout = wibox.layout.align.horizontal },
        rspace0,
       --  theme.weather.icon,
        --theme.weather.widget,
        rspace1,
        wificon,
        rspace0,
        volicon,
        rspace2,
        baticon,
        rspace3,
        wibox.widget.systray(),
    },
}

s.mybottomwibox:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
        layout = wibox.layout.fixed.horizontal,
        mylauncher,
    },
    s.mytasklist, -- Middle widget
    { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        spr_bottom_right,
        bottom_bar,
        -- cpu_icon,
        -- cpuwidget,
        bottom_bar,
        -- calendar_icon,
        -- calendarwidget,
        bottom_bar,
        -- clock_icon,
    },
}